# billeteravirtual-backend

Cambios en dev para main 2

## Billetera virtual 

Como proyecto integrador se propone el desarrollo de una billetera virtual. Un usuario debe poder crear una cuenta y logearse en nuestro sistema para lo cual se solicitan los siguientes datos: Nombre y apellido (String), email(String), dirección(String), fechaNacimiento(Date), dni(tipo y numero).
El usuario tendra una billetera asociada a su nombre. La billetera contendra un saldo, un historial de transacciones, una lista de tarjetas, una lista de contactos frecuentes (otros usuarios).

## Criterios de aceptación

* La billetera virtual debe permitir a los usuarios realizar recargas de saldo y pagos en línea utilizando diferentes métodos de pago, como tarjeta de crédito, débito, transferencia bancaria, etc.
* Los usuarios deben poder ver el historial de transacciones realizadas utilizando la billetera virtual.
* Los pagos realizados deben ser seguros y proteger la información de pago de los usuarios.
* La billetera virtual debe enviar una confirmación de transacción al usuario después de realizar un pago.
* La billetera debe permitir guardar informacion de los metodos de pago utilizados 


## Registricciones
* El email es un dato obligatorio para poder crear una cuenta.
* No puede haber mas de dos ususarios con el mismo email.
* No puede haber un usuario con mas de dos billeteras.



## Casos de Usos
* Crear una cuenta y billetera.
* Iniciar sesion en la aplicacion.
* Cerrar sesion en la aplicacion.
* Cambiar la contraseña.
* Consultar datos de usuario
* Actualizar datos de usuario
* Consultar lista de tarjetas asociadas a la cuenta.
* Agregar o Asociar una nueva tarjeta de credito y/o debito
* Eliminar una tarjeta de credito y/o debito
* Consultar saldo actual.
* Recargar Saldo.
* Retirar dinero en efectivo.
* Pagar una factura con codigo qr.
* Transeferir saldo a otra billetera
* Consultar la lista de contactos.
* Agregar un nuevo contacto
* Eliminar un ontacto de la lista de contactos.
* Consultar historial de movimientos y/o transacciones


![Diagrama de Clases](dc.png)
